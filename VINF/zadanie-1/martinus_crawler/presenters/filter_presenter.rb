require 'byebug'

class FilterPresenter
  attr_accessor :results, :params

  def initialize(elastic_results, params)
    self.results = elastic_results
    self.params  = params
  end

  def max_price
    @results[:min_max]['aggregations']['max_price']['value'].to_i
  end

  def min_price
    @results[:min_max]['aggregations']['min_price']['value'].to_i
  end

  def price_value
    @params[:price]&.split(',')&.map(&:to_i) || [min_price, max_price]
  end

  def max_weight
    @results[:min_max]['aggregations']['max_weight']['value'].to_i
  end

  def min_weight
    @results[:min_max]['aggregations']['min_weight']['value'].to_i
  end

  def weight_value
    @params[:weight]&.split(',')&.map(&:to_i) || [min_weight, max_weight]
  end

  def max_pages
    @results[:min_max]['aggregations']['max_pages']['value'].to_i
  end

  def min_pages
    @results[:min_max]['aggregations']['min_pages']['value'].to_i
  end

  def pages_value
    @params[:pages]&.split(',')&.map(&:to_i) || [min_pages, max_pages]
  end

  def max_year
    Date.today.year
  end

  def min_year
    @results[:min_max]['aggregations']['min_year']['value'].to_i
  end

  def year_value
    @params[:year]&.split(',')&.map(&:to_i) || [min_year, max_year]
  end

  def bucket_count(name)
    counts = @results[:availability]['aggregations']['availability']['buckets'].map{|o| [o['key_as_string'],o['doc_count']]}.to_h

    return counts['true'] || 0 if name == :is_available
    counts['false'] || 0
  end

  def tags
    names = get_bucket_names 'tagss'
    counts = get_buckets_counts @results[:tags], 'tags'

    fasets names, counts
  end

  def distributors
    names = get_bucket_names 'distributors'
    counts = get_buckets_counts @results[:distributors], 'distributors'

    fasets names, counts
  end

  def languages
    names = get_bucket_names 'languages'
    counts = get_buckets_counts @results[:languages], 'languages'

    fasets names, counts
  end

  def packages
    names = get_bucket_names 'packagings'
    counts = get_buckets_counts @results[:packagings], 'packagings'

    fasets names, counts
  end

  def sort_options
    {
      'relevance': 'Most relevant',
      'price asc': 'Cheapest',
      'price desc': 'Most Expensive'
    }
  end

  private

  def fasets(names, counts)
    names.map do |name|
      count = counts[name] || 0
      [name, count]
    end.to_h
  end

  def get_bucket_names(bucket_name)
    @results[:all_sets]['aggregations'][bucket_name]['buckets'].map{|o| o['key']}
  end

  def get_buckets_counts(bucket, bucket_name)
    bucket['aggregations'][bucket_name]['buckets'].map{|o| [o['key'], o['doc_count']]}.to_h
  end
end
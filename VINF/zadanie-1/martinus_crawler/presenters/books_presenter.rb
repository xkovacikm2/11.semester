require 'json'

class BooksPresenter
  attr_accessor :results

  def initialize(results)
    self.results = results
  end

  def total
    @results['hits']['total']
  end

  def books
    @results['hits']['hits'].map { |book| book['_source'] }
  end
end
require 'byebug'

class SuggestPresenter
  attr_accessor :suggestions

  def initialize(suggestions)
    self.suggestions = suggestions
  end

  def get_suggestions
    results = {}
    results['Authors'] = parse_suggest 'author_suggest'
    results['Titles'] = parse_suggest 'title_suggest'

    results
  end

  private

  def parse_suggest(name)
    @suggestions['suggest'][name].map { |suggest| "#{suggest['text']} => #{suggest['options'].map{|o| o['text']}.join(',')}" }
  end
end
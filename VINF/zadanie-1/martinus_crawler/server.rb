require 'sinatra'
require 'haml'
require_relative 'presenters/filter_presenter'
require_relative 'presenters/books_presenter'
require_relative 'presenters/suggest_presenter'
require_relative 'services/search_service'

get '/' do
  perform_search(params)
  set_presenters params

  haml :index
end

def perform_search(params)
  search_service = SearchService.new(params)
  @results = search_service.search
  @fasets_data = search_service.fetch_fasets_data
  @suggestions = params[:query].nil? || params[:query].empty? ? {} : search_service.fetch_suggestions
end

def set_presenters(params)
  @filter_presenter = FilterPresenter.new @fasets_data, params
  @books_presenter = BooksPresenter.new @results
  @suggests_presenter = SuggestPresenter.new @suggestions
end
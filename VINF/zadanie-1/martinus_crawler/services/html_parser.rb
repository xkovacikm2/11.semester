require 'nokogiri'

class HtmlParser
  attr_accessor :html

  def initialize(raw_html)
    self.html = Nokogiri::HTML raw_html
  end

  def product_attributes
    attributes = {
      title: self.parse_title,
      subtitle: self.parse_subtitle,
      tags: self.parse_tags,
      price: self.parse_price,
      is_available: self.parse_availability,
      old_price: self.parse_old_price,
      author: self.parse_author,
      distributor: self.parse_distributor,
      year: self.parse_year,
      description: self.parse_description,
      pages: self.parse_pages,
      weight: self.parse_weight,
      packaging: self.parse_package,
      language: self.parse_language,
      isbn: self.parse_isbn,
      rating: self.parse_rating
    }
    attributes[:discount_percentage] = parse_discount_percentage unless attributes[:old_price].nil?

    attributes
  end

  def parse_title
    @html.css('h1').text.strip
  end

  def parse_subtitle
    @html.css('h2.product-detail__subtitle').text.strip
  end

  def parse_tags
    tags = @html.css('.bar .badges .badge').map do |tag|
      tag.text.strip
    end

    # first badge is stars
    tags[1..-1]
  end

  def parse_price
    sanitize_price @html.css('.card .price-box__price')
  end

  def parse_old_price
    sanitize_price @html.css('.card .text-strikethrough')
  end

  def parse_discount_percentage
    @html.css('.card .badges .badge--primary').first&.text&.match(/([0-9]+)/).to_s.to_i
  end

  def parse_availability
    !@html.css('.mj-delivery-time span').empty?
  end

  def parse_author
    @html.css('ul.product-detail__author a').first&.text
  end

  def parse_distributor
    distributor_year.first
  end

  def parse_year
    distributor_year.last.to_i
  end

  def parse_description
    @html.css('section#description .cms-article').children.reject{ |child| child.name == 'script' }.map(&:text).join('')
  end

  def parse_pages
    details_list('počet stran')&.to_i
  end

  def parse_weight
    details_list('hmotnost')&.to_i
  end

  def parse_package
    details_list 'vazba'
  end

  def parse_language
    details_list 'jazyk'
  end

  def parse_isbn
    details_list 'isbn'
  end

  def parse_rating
    reviews = html.css('header.review__header.bar .rating-star.bar__item')

    reviews.map do |review|
      review.css('svg.icon.icon-star.is-active').size
    end
  end

  private

  def sanitize_price(price_dom)
    price_dom.text.strip.split(' ').first.to_i rescue nil # not space but &nbsp;
  end

  def distributor_year
    @html.css('.bar .bar__item dl dd').first.text.strip.split(', ')
  end

  def details_list(key)
    @html.css('.card.card--well dl').select{|dl| dl.css('dt').text.downcase.include? key.downcase}.first&.css('dd')&.text
  end
end
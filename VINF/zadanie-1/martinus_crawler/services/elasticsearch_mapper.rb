class ElasticsearchMapper
  def self.wrap_for_bulk(product)
    {
      index: {
        _index: :books,
        _type: :doc,
        data: product
      }
    }
  end

  def self.mapping
    {
      settings: {
        analysis: {
          filter: {
            czech_stop: {
              type: "stop",
              stopwords: "_czech_"
            },
            czech_stemmer: {
              type: "stemmer",
              language: "czech"
            }
          },
          analyzer: {
            my_czech_analyzer: {
              type: :custom,
              tokenizer: :standard,
              char_filter: [
                :html_strip
              ],
              filter: [
                :lowercase,
                :asciifolding,
                :czech_stemmer,
                :czech_stop
              ]
            },
            my_title_analyzer: {
              type: :custom,
              tokenizer: :standard,
              filter: [
                :lowercase,
                :asciifolding,
              ]
            }
          }
        }
      },
      mappings: {
        doc: {
          properties: {
            title: {
              type: :text,
              analyzer: :my_title_analyzer
            },
            subtitle: {
              type: :text,
              analyzer: :my_title_analyzer
            },
            tags: {
              type: :keyword,
            },
            price: {
              type: :integer
            },
            is_available: {
              type: :boolean
            },
            old_price: {
              type: :integer
            },
            author: {
              type: :text,
              analyzer: :my_title_analyzer
            },
            distributor: {
              type: :keyword,
            },
            year: {
              type: :integer
            },
            description: {
              type: :text,
              analyzer: :my_czech_analyzer
            },
            pages: {
              type: :integer
            },
            weight: {
              type: :integer
            },
            packaging: {
              type: :keyword,
            },
            language: {
              type: :keyword,
            },
            isbn: {
              type: :keyword
            },
            rating: {
              type: :integer
            }
          }
        }
      }
    }
  end
end
require_relative 'downloaders/category_links_downloader.rb'
require_relative 'downloaders/product_links_downloader.rb'
require_relative 'downloaders/product_pages_downloader.rb'

url = ARGV[0]

category_downloader = Downloaders::CategoryLinksDownloader.new url
categories = category_downloader.fetch

links = categories.map do |category, link|
  Downloaders::ProductLinksDownloader.new(category, link).fetch
end.flatten

product_pages_downloader = Downloaders::ProductPagesDownloader.new
product_pages_downloader.download links

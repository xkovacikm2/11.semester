require 'nokogiri'
require 'json'
require 'open-uri'
require_relative '../downloaders'
require_relative 'abstract_links_downloader'

module Downloaders
  class ProductLinksDownloader < AbstractLinksDownloader
    attr_accessor :category_name, :category_url, :links

    def initialize(category_name, category_url)
      self.category_url = category_url
      self.category_name = category_name
      self.dump_file = "#{DATA_LINKS_FOLDER}/product/#{@category_name}.json"
      self.links = []
    end

    private

    def download
      url = @category_url
      idx = 1

      until url.nil?
        html = Nokogiri::HTML open("http://#{url}")
        @links += fetch_products_links html
        url = fetch_next_page_link html

        if idx % 10 == 0
          puts "#{@category_name} links from #{idx} pages"
        end

        idx += 1
      end

      @links
    end

    def fetch_products_links(html)
      html.css('.product-item').css('a.link--product').map do |link|
        sanitize_url link.attribute('href').value
      end
    end

    def fetch_next_page_link(html)
      button = html.css('.pagination a').last
      return nil unless button&.attr('rel') == 'next'

      sanitize_url button.attr('href')
    end
  end
end

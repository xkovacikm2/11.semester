require 'nokogiri'
require 'json'
require 'open-uri'
require_relative '../downloaders'
require_relative 'abstract_links_downloader'

module Downloaders
  class CategoryLinksDownloader < AbstractLinksDownloader
    attr_accessor :uri

    def initialize(uri)
      self.uri = uri
      self.dump_file = "#{DATA_LINKS_FOLDER}/#{@uri}.json"
    end

    private

    def download
      html = Nokogiri::HTML open("http://#{@uri}")
      html.css('#mega-menu-knihy .mega-menu-categories a.mega-menu-categories__title').map do |link|
        [link.text.strip, sanitize_url(link.attribute('href').value)]
      end.to_h
    end
  end
end

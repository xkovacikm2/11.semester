require 'open-uri'
require_relative '../downloaders'

module Downloaders
  class ProductPagesDownloader

    def download(page_links)
      idx = 1

      page_links.each do |link|
        id = link.split('=').last
        filename = "#{DATA_PAGES_FOLDER}/#{id}.html"

        unless File.exists? filename
          begin
            raw_page = open "http://#{link}"
            File.write filename, raw_page.read
          rescue Exception => e
            puts e.message
          end

          if idx % 10 == 0
            puts "serialized #{idx}"
          end

          idx += 1
        end
      end
    end

  end
end

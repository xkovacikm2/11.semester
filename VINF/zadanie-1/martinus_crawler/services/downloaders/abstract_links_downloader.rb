require 'json'

module Downloaders
  class AbstractLinksDownloader
    attr_accessor :dump_file

    def fetch
      if File.file? dump_file
        links = JSON.parse File.read(dump_file)
      else
        links = download
        File.write(dump_file, JSON.pretty_generate(links))
      end

      links
    end

    protected

    def sanitize_url(url)
      url.split('//').last
    end
  end
end

require 'elasticsearch'

require_relative 'html_parser'
require_relative 'elasticsearch_mapper'

PAGES_FOLDER = '../data/pages'

file_names = Dir["#{PAGES_FOLDER}/*.html"]

products = []

file_names.each do |file_name|
  raw_html = File.read file_name
  parser = HtmlParser.new raw_html
  product = parser.product_attributes

  products << ElasticsearchMapper.wrap_for_bulk(product)
end

elastic_client = Elasticsearch::Client.new log: true
elastic_client.indices.create index: 'books', body: ElasticsearchMapper.mapping
elastic_client.bulk body: products

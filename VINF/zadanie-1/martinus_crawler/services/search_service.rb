require 'elasticsearch'

class SearchService
  attr_accessor :params, :client, :price_from, :price_to, :weight_from, :weight_to,
                :pages_from, :pages_to, :year_from, :year_to

  def initialize(params)
    self.params = params

    unless params.empty?
      self.price_from, self.price_to = split_ranges(@params[:price])
      self.weight_from, self.weight_to = split_ranges(@params[:weight])
      self.pages_from, self.pages_to = split_ranges(@params[:pages])
      self.year_from, self.year_to = split_ranges(@params[:year])
    end

    self.client = Elasticsearch::Client.new log: true
  end

  def search
    query = create_search_query
    @client.search index: :books, body: query
  end

  def fetch_fasets_data
    results = {}

    results[:min_max] = @client.search index: :books, body: min_max_query
    results[:availability] = @client.search index: :books, body: availability_faset_query
    results[:tags] = @client.search index: :books, body: tags_faset_query
    results[:distributors] = @client.search index: :books, body: distributor_faset_query
    results[:languages] = @client.search index: :books, body: language_faset_query
    results[:packagings] = @client.search index: :books, body: packaging_faset_query
    results[:all_sets] = @client.search index: :books, body: all_sets_query

    results
  end

  def fetch_suggestions
    query = {
      suggest: {
        text: params[:query],
        author_suggest: {
          term: {
            field: :author
          }
        },
        title_suggest: {
          term: {
            field: :title
          }
        }
      }
    }

    @client.search index: :books, body: query
  end

  private

  def create_search_query
    return {} if params.empty?

    query = {
      query: {
        bool: {
          must: ranges_query + availability_query + select_query(:tag) + select_query(:distributor) + select_query(:language) + select_query(:packaging) + [
            bool: {
              should: title_query + author_query + isbn_query + fulltext_query
            }
          ]
        }
      },
      highlight: {
        fields: {
          title: {},
          subtitle: {},
          author: {},
          description: {}
        }
      }
    }

    query[:sort] = sort_query unless params[:sort] == 'relevance'

    query
  end

  def min_max_query
    query = create_search_query
    query[:aggs] = {
      min_price: { min: { field: 'price' } },
      max_price: { max: { field: 'price' } },
      min_weight: { min: { field: 'weight' } },
      max_weight: { max: { field: 'weight' } },
      min_pages: { min: { field: 'pages' } },
      max_pages: { max: { field: 'pages' } },
      min_year: { min: { field: 'year' } },
      max_year: { max: { field: 'year' } },
    }

    query
  end

  def availability_faset_query
    query = params.empty? ? {} : {
      query: {
        bool: {
          must: ranges_query + select_query(:tag) + select_query(:distributor) + select_query(:language) + select_query(:packaging) + [
            bool: {
              should: title_query + author_query + isbn_query + fulltext_query
            }
          ]
        }
      }
    }

    query[:aggs] = {
      availability: {
        terms: {
          field: :is_available
        }
      }
    }

    query
  end

  def tags_faset_query
    query = params.empty? ? {} : {
      query: {
        bool: {
          must: ranges_query + availability_query + select_query(:distributor) + select_query(:language) + select_query(:packaging) + [
            bool: {
              should: title_query + author_query + isbn_query + fulltext_query
            }
          ]
        }
      }
    }

    query[:aggs] = {
      tags: {
        terms: {
          field: :tags,
          size: 20
        }
      }
    }

    query
  end

  def distributor_faset_query
    query = params.empty? ? {} : {
      query: {
        bool: {
          must: ranges_query + availability_query + select_query(:tags) + select_query(:language) + select_query(:packaging) + [
            bool: {
              should: title_query + author_query + isbn_query + fulltext_query
            }
          ]
        }
      }
    }

    query[:aggs] = {
      distributors: {
        terms: {
          field: :distributor,
          size: 1000
        }
      }
    }

    query
  end

  def packaging_faset_query
    query = params.empty? ? {} : {
      query: {
        bool: {
          must: ranges_query + availability_query + select_query(:tags) + select_query(:distributor) + select_query(:language) + [
            bool: {
              should: title_query + author_query + isbn_query + fulltext_query
            }
          ]
        }
      }
    }

    query[:aggs] = {
      packagings: {
        terms: {
          field: :packaging
        }
      }
    }

    query
  end

  def language_faset_query
    query = params.empty? ? {} : {
      query: {
        bool: {
          must: ranges_query + availability_query + select_query(:tags) + select_query(:distributor)  + select_query(:packaging) + [
            bool: {
              should: title_query + author_query + isbn_query + fulltext_query
            }
          ]
        }
      }
    }

    query[:aggs] = {
      languages: {
        terms: {
          field: :language
        }
      }
    }

    query
  end

  def all_sets_query
    query = { aggs: {} }

    %i(tags distributor language packaging).each do |bucket|
      query[:aggs][:"#{bucket}s"] = {
        terms: {
          field: bucket
        }
      }
    end

    query
  end

  def sort_query
    split = params[:sort].split(' ')

    {
      split[0] => split[1]
    }
  end

  def availability_query
    query = []

    if !!params[:is_available]
      query.push({
                   term: {
                     is_available: true
                   }
                 })
    end
    if !!params[:not_is_available]
      query.push({
                   term: {
                     is_available: false
                   }
                 })
    end

    [{ bool: {
      should: query
    } }]
  end

  def select_query(selects)
    return [] if params[:"#{selects}s"].nil?

    [{
       terms: {
         selects => params[:"#{selects}s"].keys.map(&:to_s)
       }
     }]
  end

  def ranges_query
    [
      {
        bool: {
          must: [
            {
              range: {
                price: {
                  gte: @price_from,
                  lte: @price_to
                }
              }
            },
            {
              range: {
                weight: {
                  gte: @weight_from,
                  lte: @weight_to
                }
              }
            },
            {
              range: {
                year: {
                  gte: @year_from,
                  lte: @year_to
                }
              }
            },
            {
              range: {
                pages: {
                  gte: @pages_from,
                  lte: @pages_to
                }
              }
            }
          ]
        }
      }
    ]
  end

  def title_query
    [
      {
        term: {
          title: {
            value: @params[:query],
            boost: 10
          }
        }
      },
      {
        match_phrase: {
          title: {
            query: @params[:query],
            boost: 5
          }
        },
      },
      {
        match_phrase_prefix: {
          title: {
            query: @params[:query],
            boost: 4
          }
        }
      }
    ]
  end

  def author_query
    [
      {
        term: {
          author: {
            value: @params[:query],
            boost: 4
          }
        }
      },
      {
        match: {
          author: {
            query: @params[:query],
            boost: 3
          }
        },
      },
    ]
  end

  def isbn_query
    [
      {
        term: {
          isbn: {
            value: @params[:query],
            boost: 10
          }
        }
      },
      {
        prefix: {
          isbn: {
            value: @params[:query],
            boost: 8
          }
        }
      }
    ]
  end

  def fulltext_query
    [
      {
        multi_match: {
          query: @params[:query],
          fields: ['title^3', 'subtitle^2', 'description']
        }
      }
    ]
  end

  def split_ranges(range)
    split_range = range.split ','
    return split_range[0], split_range[1]
  end
end
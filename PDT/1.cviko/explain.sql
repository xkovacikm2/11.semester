explain(format yaml) select * from documents;
-- seq scan - citam vsetky zaznamy (nechcem, lebo citam zo suboru, ktory moze byt velky)
-- plan rows: cca kolko zaznamov to musi precitat
-- startup cost: co ma stoji aby som vobec s query zacal (kedze idem sekvencne, tak zadarmo)
-- total cost: bezrozmerna velicina, ktoru sa snazim minimalizovat. priblizne pocet citani z disku
-- -- pocita sa ako (disc_page_reads * 1) + (rows * 0.01)
-- plan width: priemerny pocet bytov na riadok

explain(format yaml) select * from documents where supplier = 'SPP';

explain(format yaml) select * from documents where supplier = 'SPP' order by created_at desc;

explain(format yaml) select * from documents where supplier = 'SPP' order by created_at desc limit 10 offset 15000;
-- limit je len counter nad streamom a ked dojde hranica, tak stopne a odosle data

explain(format yaml, analyze true) select * from documents;

select relname, reltuples, relpages, round(reltuples/relpages) as row_per_page from pg_class where relname = 'documents';
-- pocet stranok a pocet riadkov na stranku na disku
-- ked je vytazena db, tak toto sa nemusi stihat updatovat a potom sa vyberaju suboptimalne queries

analyze documents;
-- forcne analyzu tabulky documents, dobre obcas pustit nad vytazenou db, ked nestiha updatovat

select
tablename,attname,null_frac,avg_width,n_distinct,correlation,
most_common_vals,most_common_freqs,histogram_bounds
from pg_stats
where tablename='documents';

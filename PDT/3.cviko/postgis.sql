-- vzdialenost medzi fiit a stanicou
select st_distance(pop.way, (
  select way
  from planet_osm_polygon
  where name ilike 'Bratislava hlavná stanica'
), false) from planet_osm_polygon as pop where name ilike 'fakulta informatiky%';

--------------------------------------
-- susedia s karlovkou
select name 
from planet_osm_polygon as pop 
where st_touches(pop.way, (
	select way 
	from planet_osm_polygon 
	where name ilike 'karlova ves' 
	limit 1
));

--------------------------------------
-- ktore ulice sa napajaju na molecku

-- najde molecky
select st_buffer(pol.way::geography, 10)
from planet_osm_line as pol
where name ilike 'molecova';

-- spoj molecky do jednej a sprav buffer
select st_buffer(
	st_union(ARRAY(select way 
	 from planet_osm_line as pol
	 where name ilike 'molecova'
	))::geography, 10);

select name, way
from planet_osm_line
where st_crosses(way::geometry, (
	select st_buffer(
	st_union(ARRAY(select way 
	 from planet_osm_line as pol
	 where name ilike 'molecova'
	))::geography, 10))::geometry);

WITH RECURSIVE factorial AS (
  SELECT 1 AS val, 1 AS iter
  UNION ALL
  SELECT val*iter AS val, iter + 1 AS iter
  FROM factorial
  WHERE iter <= 10
)

SELECT iter-1, val
FROM factorial;
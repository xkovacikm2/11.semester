WITH RECURSIVE required_parts (
    SELECT *
    FROM product_parts
    WHERE name ILIKE 'chair'
    
    UNION ALL
    
    SELECT pp.*
    FROM product_parts AS pp
    JOIN required_parts AS rp ON rp.id = pp.part_of_id
)

-- all chair parts
SELECT name
FROM required_parts;

-- longest shipping part
SELECT name, shipping_time
FROM required_parts AS rp
WHERE shipping_time = (
    SELECT max(shipping_time)
    FROM required_parts
);


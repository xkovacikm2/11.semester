WITH RECURSIVE fibonacci AS (
	SELECT 1 AS value, 1 AS prev_value, 1 AS iteration
	UNION ALL
	SELECT value + prev_value AS value, value AS prev_value, iteration + 1 AS iteration
	FROM fibonacci
	WHERE iteration < 20
)

SELECT iteration, value
FROM fibonacci;
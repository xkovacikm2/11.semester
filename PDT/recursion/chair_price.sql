WITH RECURSIVE chair_price AS (
    SELECT id, part_of_id, price
    FROM product_parts
    WHERE name ILIKE 'chair'
    
    UNION ALL
    
    SELECT pp.id, pp.part_of_id, pp.price
    FROM chair_price AS cp
    LEFT JOIN product_parts AS pp ON cp.id = pp.part_of_id
    WHERE pp.id IS NOT NULL
)

SELECT SUM(cp.price)
FROM chair_price AS cp
LEFT JOIN product_parts AS pp ON cp.id = pp.part_of_id
WHERE pp.id IS NULL;
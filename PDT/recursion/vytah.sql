WITH RECURSIVE get_components AS (
    SELECT *
    FROM komponenty
    WHERE dil = 'kabina_vytahu'
    
    UNION ALL
    
    SELECT k.dil, k.komponenta, k.mnozstvi * gc.mnozstvi
    FROM get_components AS gc
    JOIN komponenty AS k ON gc.komponenta = k.dil
)

SELECT SUM(mnozstvi)
FROM get_components
WHERE komponenta = 'pant';
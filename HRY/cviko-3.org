* Game idea
** core idea
Nepriatelske delostrelectvo utoci na vase mesto projektilmi. Ulohou hraca je
tieto projektily odklonit tak, aby nezasiahli ziadnu budovu v meste a dopadli do
vyhradenej "safe" zony. Projektil hrac vie odklanat stavanim plosin, od ktorych
sa bude projektil odrazat.
- vyzorovo iny projektil moze mat ine spravanie
  - chodi do L ako kon v sachu
  - je prilis rychly a odrazacie plosiny by znicil a treba ho spomalit najprv
- vyzorovo ine plosiny maju ine spravanie
  - uhol dopadu == uhol odrazu
  - otocenie o 90% 
  - spomalenie
** controls
- vyber typu plosiny: mys, klavesove skratky (1-9)
- vyber pozicie plosiny: mys
- vyber natocenia plosiny: sipky na klavesnici?
- umiestnenie plosiny: left click alebo enter?
- spustenie delostrelby: space, right click?
** sketch
[[file:misc/game_proto_sketch.jpg]]
** genre, platform, target audience
- 2D puzzle/labyrinth
- najprv PC, potom port na mobil?
- target audience -> hraci bridge buildera, logickych hier
- biznis model -> predaj napovedy, predaj dalsich kociek?
** closest competitor
- mind reflection
- bridge builder
** unique selling point
- namiesto nicenia sa snazim chranit

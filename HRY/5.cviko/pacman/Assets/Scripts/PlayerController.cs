﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public float speed = 10f;
  public int score = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
  		if(Input.GetKey(KeyCode.W)){
        this.GetComponent<Transform>().position += Vector3.forward * Time.deltaTime * speed;
      } else if (Input.GetKey(KeyCode.S)){
        this.GetComponent<Transform>().position += Vector3.back * Time.deltaTime * speed;
      } else if (Input.GetKey(KeyCode.A)){
        this.GetComponent<Transform>().position += Vector3.left * Time.deltaTime * speed;
      } else if (Input.GetKey(KeyCode.D)){
        this.GetComponent<Transform>().position += Vector3.right * Time.deltaTime * speed;
      }
    }
}

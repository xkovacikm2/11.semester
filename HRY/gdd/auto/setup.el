(TeX-add-style-hook
 "setup"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted"
    "pdfpages")
   (TeX-add-symbols
    "Author"
    "Title"
    "Subtitle"
    "Examiner"
    "Year"
    "Month")
   (LaTeX-add-labels
    "sec:org129b891"
    "sec:org4c8b509"
    "sec:orgd532b33"
    "sec:org6296700"
    "sec:org3279e67"
    "sec:orgc820e55"
    "sec:org7286a1b"
    "sec:org4b5ccbf"
    "sec:org491a41a"
    "sec:orgf6fda4c"
    "sec:orgc7077a1"
    "sec:orgce5c1bd"
    "sec:org5e7b692"
    "sec:orgd5f33ed"
    "sec:orgf0ad9b0"
    "sec:org49fe905"
    "sec:orgcb499d4"
    "sec:org489e540"
    "sec:org3a3c801"
    "sec:org590fdbf"
    "fig:orge2d5cb2"
    "fig:org4f1d4e0"
    "fig:org8d10b7b"
    "fig:org947b26d"))
 :latex)

